
## 0.3.9 [10-14-2024]

* Changes made at 2024.10.14_19:41PM

See merge request itentialopensource/adapters/adapter-git!14

---

## 0.3.8 [09-24-2024]

* Removed request and patch-package dependencies

See merge request itentialopensource/adapters/adapter-git!12

---

## 0.3.7 [08-26-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-git!11

---

## 0.3.6 [08-14-2024]

* Changes made at 2024.08.14_17:46PM

See merge request itentialopensource/adapters/adapter-git!10

---

## 0.3.5 [08-07-2024]

* Changes made at 2024.08.06_18:42PM

See merge request itentialopensource/adapters/adapter-git!9

---

## 0.3.4 [07-31-2024]

* manual migration updates

See merge request itentialopensource/adapters/devops-netops/adapter-git!8

---

## 0.3.3 [03-26-2024]

* Changes made at 2024.03.26_14:21PM

See merge request itentialopensource/adapters/devops-netops/adapter-git!7

---

## 0.3.2 [03-13-2024]

* Changes made at 2024.03.11_13:20PM

See merge request itentialopensource/adapters/devops-netops/adapter-git!6

---

## 0.3.1 [01-08-2024]

* remove the fix resolution and metadata issues

See merge request itentialopensource/adapters/devops-netops/adapter-git!4

---

## 0.3.0 [01-06-2024]

* 2023 migration changes

See merge request itentialopensource/adapters/devops-netops/adapter-git!3

---

## 0.2.0 [03-09-2023]

* Add tasks to create and checkout branch

See merge request itentialopensource/adapters/devops-netops/adapter-git!1

---

## 0.1.1 [03-07-2023]

* Bug fixes and performance improvements

See commit caed158

---
