# Adapter for Git

Vendor: Generic Git
Homepage: https://en.wikipedia.org/wiki/Git

Product: Git 
Product Page: https://en.wikipedia.org/wiki/Git

## Introduction

We classify Git into the CI/CD domain as Git handles the versioning, building, storage and deploying of the system and components.

## Why Integrate
The Git adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Git. With this adapter you have the ability to perform operations with Git on items such as:

- Manage projects and files in a repository

## Additional Product Documentation
The [Git Node Library Documentation](https://www.npmjs.com/package/isomorphic-git)
