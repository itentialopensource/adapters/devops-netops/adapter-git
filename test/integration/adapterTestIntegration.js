/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const mocha = require('mocha');
const path = require('path');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const isRapidFail = false;
const isSaveMockData = false;
const attemptTimeout = 10000;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
const host = 'localhost';
const username = 'username';
const password = 'password';
const sslenable = false;
const sslinvalid = false;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-Git',
      type: 'Git',
      stub: true,
      properties: {
        authentication: {
          username: 'username',
          password: 'password'
        }
      }
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

const Git = require('../../adapter');

describe('[integration] Git Adapter Test', () => {
  describe('Git Class Tests', () => {
    const a = new Git(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        assert.notEqual(null, a);
        assert.notEqual(undefined, a);
        const check = global.pronghornProps.adapterProps.adapters[0].id;
        assert.equal(check, a.id);
        done();
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      if (!stub) {
        it('should get connected', (done) => {
          a.connect();
          assert.equal(true, a.alive);
          done();
        }).timeout(attemptTimeout);
      }
    });

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    const cloneOptions = {
      dir: '../../test',
      url: 'https://gitlab.com/test/test-project.git'
    };
    describe('#cloneRepo', () => {
      if (!stub) {
        it('should clone the repo', (done) => {
          try {
            a.cloneRepo(cloneOptions, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const branchOptions = {
      dir: '../../test',
      ref: 'patch/test'
    };
    describe('#createBranch', () => {
      if (!stub) {
        it('should create a new branch', (done) => {
          try {
            a.createBranch(branchOptions, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const checkoutOptions = {
      dir: '../../test',
      ref: 'patch/test'
    };
    describe('#checkoutBranch', () => {
      if (!stub) {
        it('should checkout a branch', (done) => {
          try {
            a.checkoutBranch(checkoutOptions, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const removeOptions = {
      dir: '../../test',
      filepath: 'README.md'
    };
    describe('#removeFile', () => {
      if (!stub) {
        it('should remove file', (done) => {
          try {
            a.removeFile(removeOptions, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const options = {
      dir: '../../test',
      filepath: 'README.md'
    };
    describe('#getStatus', () => {
      if (!stub) {
        it('should get status of a file', (done) => {
          try {
            a.getStatus(options, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const matrixOptions = {
      dir: '../../test'
    };
    describe('#getStatusMatrix', () => {
      if (!stub) {
        it('should get status of multiple files', (done) => {
          try {
            a.getStatusMatrix(matrixOptions, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const commitOptions = {
      dir: '../../test',
      message: 'delete README',
      author: {
        name: ''
      }
    };
    describe('#createCommit', () => {
      if (!stub) {
        it('should create commit', (done) => {
          try {
            a.createCommit(commitOptions, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });

    const pushOptions = {
      dir: '../../test',
      remote: 'origin',
      ref: 'main'
    };
    describe('#pushChanges', () => {
      if (!stub) {
        it('should push a branch', (done) => {
          try {
            a.pushChanges(pushOptions, (data, error) => {
              try {
                runCommonAsserts(data, error);
                assert.equal(200, data.code);
                done();
              } catch (ex) {
                log.error(`Test Failure: ${ex}`);
                done(ex);
              }
            });
          } catch (exc) {
            log.error(`Adapter Exception: ${exc}`);
            done(exc);
          }
        }).timeout(attemptTimeout);
      }
    });
  });
});
